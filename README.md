# Angular Wagtail

Integrate a "headless" Wagtail CMS with Angular

## Features

- Match Wagtail Page Types to Angular Components
- Support lazy loaded components and lazy loaded modules
- Support Wagtail's preview feature thanks to wagtail-headless-preview
- Support Wagtail's SEO meta tag features
- Support for Angular Universal
  - Supports proper 404, 301, and 302 status codes from Node
- Show custom 404 page when no CMS page
- Support Multiple Wagtail sites

## Assumptions

- Assumes Wagtail and Angular are separate and that Angular is running as a single page application

# Installation

1. Install `angular-wagtail` with npm or yarn.
2. Install `@nguniversal/express-engine` even if not using angular-universal. Angular Wagtail uses some injection tokens from it.
3. Add preview and catchall route to your Routes

```typescript
import { WagtailComponent, GetPageDataResolverService } from 'angular-wagtail';
...
  {
    path: 'preview',
    component: WagtailComponent,
    resolve: { cmsData: GetPageDataResolverService },
    runGuardsAndResolvers: 'always',
    data: { preview: true },
  },
  {
    path: '**',
    component: WagtailComponent,
    resolve: { cmsData: GetPageDataResolverService },
    runGuardsAndResolvers: 'always',
  },
```

The `preview` path may be anything, but it needs to match your Django server's HEADLESS_PREVIEW_CLIENT_URLS path

4. Add WagtailModule to your root AppModule. Example:

```typescript
import { WagtailModule } from 'angular-wagtail';
import { NotFoudPageComponent } from './my-app/not-found.component.ts'
...
  WagtailModule.forRoot({
    apiDomain: 'http://localhost:8000',
    siteId: 2,
    notFoundComponent: NotFoundPageComponent,
    setCanonicalURL: true,
    pageTypes: [
      {
        type: 'sandbox.FooPage',
        loadComponent: () =>
          import('./lazy-foo-page/lazy-foo-page.component').then(
            (x) => x.LazyFooPageComponent
          ),
      },
      {
        type: 'sandbox.BarPage',
        component: BarPageComponent,
      },
    ],
  }),
```

6. If using Universal, ensure app.server.module.ts has `ServerTransferStateModule`
7. Configure WagtailModule.forRoot, which accepts the following configuration object.

- pagesApiPath - Set this to the path (without domain) of your wagtail pages API endpoint. For example /api/v2/pages/ (default)
- notFoundComponent - A component to display when a page 404's.
- wagtailSiteId - Filter only by this wagtail site. Alternatively, if using Node, set the environment variable `WAGTAIL_SITE_ID`.
- excludeApiTypes - Wagtail Page types to exclude. If you had 1020 pages and 1000 of them were blog posts, you could exclude the blog posts.I alwa
- pageTypes - An array of objects that match Wagtail Page Types to Angular Components. It's similar to Angular Router. Example:

```typescript
[
  {
    type: "cms.MyGreatPage",
    component: MyGreatComponent,
    type: "cms.LazyLoadedPage",
    loadChildren: "./lazy-loaded/lazy.module#LazyModule",
  },
];
```

- setCanonicalURL = (Optional) Set to `true` to have Angular set the Canonical URL link tag to the wagtail page's html_url. You may need to force Wagtail to show the html_url with a https by setting the Wagtail Site's port to 443.

## Lazy Loaded Components and Modules Support

Angular's Ivy engine supports lazy loaded components in addition to modules. We recommend using single component modules like this. The module may be omitted entirely if the component doesn't have any module dependencies. Keep the component and module in the same file.

```typescript
@Component({
  template: ` <h2>Example</h2> `,
})
export class LazyPageComponent {}

@NgModule({
  declarations: [LazyPageComponent],
  imports: [MyNeededModule],
})
export class LazyPageModule {}
```

### Fetching CMS Data with a resolver

Typically Wagtail page data needs displayed on the component. This is done via an Angular router resolver. It works just like any resolver. We suggest resolving this to "cmsData".

```typescript
import { ActivatedRoute } from "@angular/router";
import { WagtailPageDetail } from "angular-wagtail";

@Component({
  template: ` <h2>{{ cmsData?.title }}</h2> `,
})
export class LazyFooPageComponent implements OnInit {
  cmsData: WagtailPageDetail | undefined;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.cmsData = data.cmsData;
    });
  }
}
```

## Environment Variables

If using Angular Universal and serving from node - you may control some configuration variables via environment variables.

- `CMS_DOMAIN` - The domain of the CMS to use. For example cms.example.com
- `WAGTAIL_SITE_ID` - Overrides wagtailSiteId forcing Node to always use this ID. This can be useful as Node isn't easily able to determine the user's domain and Angular Universal does not support doing so.

## sitemap.xml support

Wagtail SPA Integration comes with a extended wagtail sitemap.xml feature to explicitly set the site by id. This can be called from the SSR server.ts file.

```typescript
const request = require("request");
const WAGTAIL_SITE_ID = process.env.WAGTAIL_SITE_ID || null;
const CMS_DOMAIN = process.env.CMS_DOMAIN || null;

app.get("/sitemap.xml", (req, res) => {
  if (WAGTAIL_SITE_ID && CMS_DOMAIN) {
    const params = {
      site: WAGTAIL_SITE_ID,
    };
    const url = CMS_DOMAIN + "/sitemap.xml";
    request({ url: url, qs: params }, function (err, response, body) {
      if (err) {
        console.log(err);
        return;
      }
      res.setHeader("content-type", "application/xml");
      res.send(body);
    });
  }
});
```

# How does it work

At a high level, Angular Wagtail maps Wagtail page types to Angular Components. It does not use the Angular Router in a typical manner, because routes cannot be known at build time.

- A catchall Angular route resolves CMS data using WagtailResolver
- WagtailReseolver fetches page data from Wagtail
  - If no page is found, check for redirects
    - If a redirect is found, set the Node status code and redirect (or router.navigate if running browser side)
    - If no redirect is found, set 404 status code and show the not found component
- Resolver finds appropriate component, based on mapping Wagtail page types to components. If component is lazy loaded, it loads it now before any page is rendered.
- Route resolves and loads WagtailComponent
- WagtailComponent loads desired component based on resolver
- WagtailService sets page title, metadata, and canonical page (if configured) based on wagtail page metadata.

See Angular docs on [dynamic component loading](https://angular.io/guide/dynamic-component-loader)

# Developing this module

There is a simple Angular application provided for testing and development.
It assumes the wagtail server will be on `localhost:8000` but this can be changed by editing `proxy.conf.json`.

The [wagtail-spa-integration](https://gitlab.com/thelabnyc/wagtail-spa-integration) repo contains a sample wagtail backend which can be setup with:

```
git clone https://gitlab.com/thelabnyc/wagtail-spa-integration.git
cd wagtail-spa-integration
poetry install
poetry run python manage.py migrate
poetry run python manage.py createsuperuser
poetry run python manage.py runserver
curl -X POST http://127.0.0.1:8000/test-fixture/
```

To start the angular development server use:

```
npm i
npm start
```

## Build

`ng build angular-wagtail`

## Test

`ng test angular-wagtail`

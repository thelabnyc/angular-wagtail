# 2.2.1

- Fix preview not working when used with resolver

# 2.2.0

- Adds support for wagtail-spa-integration with wagtail-headless-preview. The old preview API will be removed in a future release.

# 2.1.0

- Angular 10 support

# 2.0.0

- Angular 9 support
- Fix issue where an extra history entry for '/' is added on initial load, causing issues when the back button is pressed

# 1.2.0

- Fixed bug where sometimes 404/302 status codes couldn't be set due to attempted usage of express engine stub file. We now declare a @nguniversal/express-engine peer dependency even though it's only used to get an injection token. For users who don't need angular-universal, the express engine must now be installed. However it should have no impact on the resultant bundle (it should not be bigger due to this change).

# 1.1.1

- Fixed bug where getRedirect was not respecting siteID config

# 1.1.0

- Data resolver service now gets the draft code from the route. This fixes a potential race condition where sometimes the draft code wasn't available when requested.

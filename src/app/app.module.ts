import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { WagtailModule } from 'angular-wagtail';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BarPageComponent } from './bar-page/bar-page.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';

@NgModule({
  declarations: [AppComponent, BarPageComponent, NotFoundPageComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    WagtailModule.forRoot({
      apiDomain: 'http://localhost:8000',
      siteId: 2,
      notFoundComponent: NotFoundPageComponent,
      setCanonicalURL: true,
      pageTypes: [
        {
          type: 'sandbox.FooPage',
          loadComponent: () =>
            import('./lazy-foo-page/lazy-foo-page.component').then(
              (x) => x.LazyFooPageComponent
            ),
        },
        {
          type: 'sandbox.BarPage',
          component: BarPageComponent,
        },
      ],
    }),
    TransferHttpCacheModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WagtailComponent, GetPageDataResolverService } from 'angular-wagtail';

const routes: Routes = [
  {
    path: 'preview',
    component: WagtailComponent,
    resolve: { cmsData: GetPageDataResolverService },
    runGuardsAndResolvers: 'always',
    data: { preview: true },
  },
  {
    path: '**',
    component: WagtailComponent,
    resolve: { cmsData: GetPageDataResolverService },
    runGuardsAndResolvers: 'always',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}

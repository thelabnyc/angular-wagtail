import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WagtailPageDetail } from 'angular-wagtail';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  template: `
    <span>Bar Page Component</span>
    <div>{{ cmsData$ | async | json }}</div>
  `,
})
export class BarPageComponent {
  cmsData$: Observable<WagtailPageDetail> = this.route.data.pipe(
    map((data) => data.cmsData)
  );

  constructor(private route: ActivatedRoute) {}
}

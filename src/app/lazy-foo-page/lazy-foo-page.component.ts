import { Component, NgModule, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSliderModule } from '@angular/material/slider';
import { MatButtonModule } from '@angular/material/button';
import { WagtailPageDetail } from 'angular-wagtail';
import { NestedComponent } from './nested/nested.component';

@Component({
  template: `
    <h2>{{ cmsData?.title }}</h2>
    <p>lazy foo page works!</p>
    <mat-slider min="1" max="100" step="1" value="1"></mat-slider>
    <button mat-raised-button color="primary">Button!</button>
    <app-nested></app-nested>
  `,
})
export class LazyFooPageComponent implements OnInit {
  cmsData: WagtailPageDetail | undefined;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.cmsData = data.cmsData;
    });
  }
}

@NgModule({
  declarations: [LazyFooPageComponent, NestedComponent],
  imports: [MatSliderModule, MatButtonModule],
})
export class LazyFooPageModule {}

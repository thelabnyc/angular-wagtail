import { Component } from '@angular/core';

@Component({
  selector: 'app-nested',
  template: '<p>This is a child component</p>',
})
export class NestedComponent {}

/*
 * Public API Surface of angular-wagtail
 */

export * from './lib/wagtail.service';
export * from './lib/wagtail.component';
export * from './lib/wagtail.module';
export * from './lib/wagtail-resolver.service';
export * from './lib/interfaces';
export { StatusCodeService } from './lib/status-code.service';

import { ModuleWithProviders, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { WagtailComponent } from './wagtail.component';
import { GetPageDataResolverService } from './wagtail-resolver.service';
import { StatusCodeService } from './status-code.service';
import { WagtailModuleConfig } from './interfaces';
import { WAGTAIL_CONFIG } from './tokens';

@NgModule({
  declarations: [WagtailComponent],
  imports: [HttpClientModule],
  providers: [StatusCodeService],
  exports: [WagtailComponent],
})
export class WagtailModule {
  static forRoot(
    config: WagtailModuleConfig
  ): ModuleWithProviders<WagtailModule> {
    return {
      ngModule: WagtailModule,
      providers: [
        GetPageDataResolverService,
        { provide: WAGTAIL_CONFIG, useValue: config },
      ],
    };
  }
}

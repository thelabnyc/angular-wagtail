import { Type } from '@angular/core';

interface WagtailMeta {
  type: string;
  detail_url: string;
  html_url: string;
  slug: string;
  first_published_at?: string;
}

export interface WagtailMetaDetail extends WagtailMeta {
  show_in_menus: boolean;
  seo_title: string;
  search_description: string;
  parent: WagtailPage | null;
}

interface WagtailPageBase {
  id: number;
  title: string;
}

export interface WagtailPage extends WagtailPageBase {
  meta: WagtailMeta;
}

export interface WagtailPageDetail extends WagtailPageBase {
  meta: WagtailMetaDetail;
}

interface WagtailResponseMeta {
  total_count: number;
}

export interface WagtailResponse {
  meta: WagtailResponseMeta;
  items: WagtailPage[];
}

export type PageType = {
  type: string;
} & (
  | {
      component: Type<any>;
    }
  | {
      loadComponent: () => Type<any> | Promise<Type<any>>;
    }
);

export interface WagtailModuleConfig {
  /* The url path of the wagtail v2 pages API endpoint. Defaults to /api/v2/pages/ */
  pagesApiPath?: string;
  /* Optional, set this to override the pagesApiPath when viewing draft. Could be useful to bypass cache */
  pagesDraftApiPath?: string;
  /* Matches wagtail page types to router components or router loadChildren */
  pageTypes: PageType[];
  /* Show this component when page is not found. If not set, router will show empty template. */
  notFoundComponent?: Type<any>;
  /*
  Limit the amount of pages returned by the wagtail api. Defaults to 50.
  Should not be above wagtail WAGTAILAPI_LIMIT_MAX
  */
  pagesApiLimit?: number;
  /* Sets the fields param for the wagtail pages api. Defaults to -first_published_at */
  pagesApiFields?: string;
  /* Exclude page type. Example: myblog.BlogPost */
  excludeApiTypes?: string;
  /* Optional, limit queries to this wagtail site ID */
  siteId?: number;
  /* Optional, use this as the wagtail domain */
  apiDomain: string;
  /* Set canonical URL from wagtail page settings */
  setCanonicalURL?: boolean;
}

export interface WagtailRedirect {
  old_path: string;
  is_permanent: boolean;
  site: number | null;
  link: string;
}

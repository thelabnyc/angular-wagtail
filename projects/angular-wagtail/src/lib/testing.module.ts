import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgModule } from '@angular/core';
import { BrowserTransferStateModule } from '@angular/platform-browser';
import { StatusCodeService } from './status-code.service';
import { WAGTAIL_CONFIG } from './tokens';
import { wagtailModuleConfig } from './test-data';

@NgModule({
  imports: [HttpClientTestingModule, BrowserTransferStateModule],
  providers: [
    StatusCodeService,
    { provide: WAGTAIL_CONFIG, useValue: wagtailModuleConfig },
  ],
})
export class TestingModule {}

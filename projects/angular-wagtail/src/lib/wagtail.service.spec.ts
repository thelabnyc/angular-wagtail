import { TestBed } from '@angular/core/testing';
import { HttpTestingController } from '@angular/common/http/testing';

import { WagtailService } from './wagtail.service';
import { wagtailPageDetail } from './test-data';
import { TestingModule } from './testing.module';

describe('WagtailService', () => {
  let service: WagtailService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingModule],
    });
    service = TestBed.inject(WagtailService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should get page for URL', () => {
    const url = '/';
    service
      .getPageForUrl(url)
      .subscribe((page) => expect(page).toEqual(wagtailPageDetail));
    const req = httpTestingController.expectOne(
      'http://example.com/api/v2/pages/detail_by_path/?html_path=%2F&site=1'
    );
    expect(req.request.method).toEqual('GET');
    req.flush(wagtailPageDetail);
    httpTestingController.verify();
  });
});

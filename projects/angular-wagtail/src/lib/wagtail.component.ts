import {
  AfterViewInit,
  Component,
  ComponentFactoryResolver,
  Injector,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { WagtailService } from './wagtail.service';

@Component({
  selector: 'lib-angular-wagtail',
  template: `<ng-container #wagtailOutlet></ng-container>`,
})
export class WagtailComponent implements AfterViewInit {
  @ViewChild('wagtailOutlet', { read: ViewContainerRef })
  // @ts-ignore
  outlet: ViewContainerRef;

  constructor(
    private wagtail: WagtailService,
    private cfr: ComponentFactoryResolver,
    private injector: Injector
  ) {}

  ngAfterViewInit() {
    this.wagtail.currentPageComponent.subscribe((component) => {
      if (component) {
        const factory = this.cfr.resolveComponentFactory(component);
        this.outlet.clear();
        this.outlet.createComponent(factory, undefined, this.injector);
      }
    });
  }
}

import { WagtailPageDetail, WagtailModuleConfig } from './interfaces';

export const wagtailPageDetail: WagtailPageDetail = {
  id: 1,
  meta: {
    type: 'sandbox.FooPage',
    detail_url: 'http://localhost:8000/api/v2/pages/1/',
    html_url: 'http://example.com',
    slug: 'home',
    show_in_menus: false,
    seo_title: '',
    search_description: '',
    first_published_at: '2021-05-05T19:16:05.738162Z',
    parent: null,
  },
  title: 'foo home',
};

export const wagtailModuleConfig: WagtailModuleConfig = {
  apiDomain: 'http://example.com',
  siteId: 1,
  setCanonicalURL: true,
  pageTypes: [],
};

import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformServer } from '@angular/common';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';
import { EMPTY, from, of } from 'rxjs';
import { exhaustMap, map } from 'rxjs/operators';
import { WagtailRedirect } from './interfaces';
import { WagtailService } from './wagtail.service';
import { StatusCodeService } from './status-code.service';

@Injectable({
  providedIn: 'root',
})
export class GetPageDataResolverService implements Resolve<unknown> {
  constructor(
    private wagtail: WagtailService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: Object,
    private statusCodeService: StatusCodeService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (route.data.preview) {
      const contentType = route.queryParams.content_type;
      const token = route.queryParams.token;
      if (contentType && token) {
        return this.wagtail.getPreviewData(contentType, token).pipe(
          exhaustMap((page) => {
            if (page) {
              return from(this.wagtail.getComponentForPage(page)).pipe(
                map(() => {
                  this.wagtail.page.next(page);
                  return page;
                })
              );
            }
            return EMPTY;
          })
        );
      }
    }

    return this.wagtail.getPageForUrl(state.url).pipe(
      exhaustMap((page) => {
        if (page === null) {
          return this.wagtail.getRedirectForUrl(state.url).pipe(
            exhaustMap((redirects) => {
              if (redirects.length > 0) {
                this.redirect(redirects[0]);
                return of(null);
              }
              this.wagtail.page.next('404');
              return of('404');
            })
          );
        }
        // We need to force any lazy loaded code to load in the resolver, but it should
        // return only the page data.
        return from(this.wagtail.getComponentForPage(page)).pipe(
          map(() => {
            this.wagtail.page.next(page);
            return page;
          })
        );
      })
    );
  }

  /**
   * On server, send redirect status code
   * On browser, change window.location
   */
  redirect(redirect: WagtailRedirect) {
    if (isPlatformServer(this.platformId)) {
      this.statusCodeService.redirect(redirect.link, redirect.is_permanent);
    } else {
      const path = redirect.link;
      if (path.startsWith('http')) {
        if (window.location.href === path) {
          console.warn('Refused to redirect to self');
        } else {
          window.location.href = path;
        }
      } else {
        this.router.navigate([path]);
      }
    }
  }
}

import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import {
  makeStateKey,
  Meta,
  Title,
  TransferState,
} from '@angular/platform-browser';
import { DOCUMENT, isPlatformServer } from '@angular/common';

import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, exhaustMap, tap } from 'rxjs/operators';
import {
  WagtailModuleConfig,
  WagtailPage,
  WagtailPageDetail,
  WagtailRedirect,
} from './interfaces';
import { WAGTAIL_CONFIG } from './tokens';
import { StatusCodeService } from './status-code.service';

const CMS_DOMAIN_KEY = makeStateKey<string>('Wagtail Domain');
const CMS_SITE_ID_KEY = makeStateKey<string>('Wagtail Site ID');

@Injectable({
  providedIn: 'root',
})
export class WagtailService {
  page = new BehaviorSubject<WagtailPageDetail | null | '404'>(null);

  currentPageComponent = this.page.pipe(
    exhaustMap((page) => {
      return this.getComponentForPage(page);
    })
  );

  /** Strip out query params and #anchor */
  sanitizePath(path: string) {
    return path.split('?')[0].split('#')[0];
  }

  getPageForUrl(url: string) {
    const pathname = this.sanitizePathname(url);
    const params = new URLSearchParams({ html_path: pathname });
    const siteId = this.getWagtailSiteID();
    if (siteId) params.set('site', siteId.toString());
    return this.http
      .get<WagtailPageDetail>(
        `${this.getWagtailDomain()}${
          this.config.pagesApiPath ?? '/api/v2/pages/detail_by_path/'
        }?${params.toString()}`
      )
      .pipe(
        tap((page) => this.setMeta(page)),
        catchError((err) => {
          if (err instanceof HttpErrorResponse && err.status === 404) {
            return of(null);
          }
          throw err;
        })
      );
  }

  /**
   * Get the Wagtail Domain
   * Set the env var CMS_DOMAIN in node to override the domain
   * Otherwise it will default to "" meaning the same domain angular runs in.
   */
  getWagtailDomain() {
    let cmsDomain = this.state.get<string>(CMS_DOMAIN_KEY, '');
    if (cmsDomain) {
      return cmsDomain;
    }
    if (this.config.apiDomain) {
      cmsDomain = this.config.apiDomain;
    } else if (isPlatformServer(this.platformId)) {
      const envCmsDomain = process.env.CMS_DOMAIN;
      if (envCmsDomain) {
        cmsDomain = envCmsDomain;
      }
    }
    this.state.set(CMS_DOMAIN_KEY, cmsDomain);
    return cmsDomain;
  }

  /** Get Wagtail Site ID which may be configured by a Node env var or from the module config  */
  getWagtailSiteID() {
    let cmsId = this.state.get<number>(CMS_SITE_ID_KEY, null as any);
    if (typeof cmsId === 'number') {
      return cmsId;
    }
    if (this.config.siteId) {
      cmsId = this.config.siteId;
    } else if (isPlatformServer(this.platformId)) {
      const envcmsId = process.env.WAGTAIL_SITE_ID;
      if (envcmsId) {
        cmsId = parseInt(envcmsId);
      }
    }
    this.state.set(CMS_SITE_ID_KEY, cmsId as any);
    return cmsId;
  }

  getRedirectForUrl(url: string) {
    const siteId = this.getWagtailSiteID();
    const params = {
      old_path: url,
      ...(siteId && {
        site: siteId.toString(),
      }),
    };
    return this.http.get<WagtailRedirect[]>(
      `${this.getWagtailDomain()}/api/redirects/`,
      { params }
    );
  }

  async getComponentForPage(page: WagtailPage | null | '404') {
    if (page === null) {
      return;
    } else if (page === '404') {
      this.statusCodeService.setStatus(404, 'Not Found');
      return this.config.notFoundComponent;
    }

    const route = this.config.pageTypes.find(
      (route) => route.type === page.meta.type
    );

    if (!route) {
      return this.config.notFoundComponent;
    }

    if ('component' in route) {
      return route.component;
    } else {
      return await route.loadComponent();
    }
  }

  /** Support for wagtail-headless-preview API */
  getPreviewData<T extends WagtailPageDetail>(
    contentType: string,
    token: string
  ): Observable<T> {
    const url = this.getWagtailDomain() + '/api/v2/page_preview/1/';
    const params = new HttpParams({
      fromObject: {
        content_type: contentType,
        token: token,
        format: 'json',
      },
    });
    return this.http.get<T>(url, { params });
  }

  private sanitizePathname(path: string) {
    return path.split(/[?#]/)[0];
  }

  /** Set title and description meta tag for SEO */
  private setMeta(data: WagtailPageDetail) {
    if (data.meta.seo_title) {
      this.title.setTitle(data.meta.seo_title);
    } else {
      this.title.setTitle(data.title);
    }
    if (data.meta.search_description) {
      this.meta.addTag({
        name: 'description',
        content: data.meta.search_description,
      });
    }
    if (this.config.setCanonicalURL) {
      this.createLinkForCanonicalURL(data.meta.html_url);
    }
  }

  private createLinkForCanonicalURL(url: string) {
    const link: HTMLLinkElement = this.doc.createElement('link');
    link.setAttribute('rel', 'canonical');
    this.doc.head.appendChild(link);
    link.setAttribute('href', url);
  }

  constructor(
    private http: HttpClient,
    private statusCodeService: StatusCodeService,
    private state: TransferState,
    private title: Title,
    private meta: Meta,
    @Inject(DOCUMENT) private doc: Document,
    @Inject(WAGTAIL_CONFIG) private config: WagtailModuleConfig,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}
}

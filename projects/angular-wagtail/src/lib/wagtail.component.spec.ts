import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WagtailComponent } from './wagtail.component';
import { TestingModule } from './testing.module';

describe('WagtailComponent', () => {
  let component: WagtailComponent;
  let fixture: ComponentFixture<WagtailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WagtailComponent],
      imports: [TestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WagtailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
